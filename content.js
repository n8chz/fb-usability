// console.log("content.js checking in");

// select the target node
var target = document.body;

// Run the option callback if the option is set to true in storage
function setupOption(optionID, callback) {
 chrome.storage.local.get(optionID, function (setting) {
   // Assume options that are not yet stored as keys are set to true
   if (Object.keys(setting).length == 0 || setting[optionID]) {
    callback();
   }
 });
}

optionCallbacks = {
 "timeline-ads": function () {
  // aria-label="Comment"
  $("[role=article]:not([aria-label^='Comment']):not(:has('.timestampContent'))").remove();
  // $("._m8c, .uiStreamSponsoredLink").closest("._5pat, ._5jmm").css("border", "4px solid red").remove();
  /*
  $("._3nlk").css("border", "3px solid #0cc");
  $(".z_hms2r4vm5").css("border", "3px solid green");
  // $("[class*="_hms"]").css("border", "3px solid red");
  $("[class^='h_hms']").css("border", "3px solid red");
  $(".uiStreamAdditionalLogging").css("background-color", "green");
  $("[id^='hyperfeed']").css("border", "3px solid #f80");
  */
  // $("._3nlk, .z_hms2r4vm5, [class^='h_hms'], .uiStreamAdditionalLogging").closest("[id^='hyperfeed']").remove();
  // $("[id^='feed_sub_title']").css("border", "3px solid orange");
  // m_hms2rclx1 e_hms2r98uo

  /*
  $("s.n_hms2r866a, .uiStreamSponsoredLink").closest("[id^='hyperfeed_story_id_']").remove();
  $("._5pcp").css("background", "2px solid green");
  $("._5lel").css("background", "2px solid orange");
  // $("._2jyu").css("background", "2px solid blue");
  $("._232_").css("background", "2px solid red");
  */

  // _5pcp _5lel _2jyu _232_
  // Concerning <s> tags:
  // non-ad: e_hms2r98uo a_hms2r4ut4 p_hms2ra-hh
  // non-ad: e_hms2r98uo a_hms2r4ut4 p_hms2ra-hh
  //     ad: e_hms2r98uo n_hms2r866a p_hms2ra-hh
 },
 "suggested-groups": function () {
  // $("._5qrt").before("<p>found us a suggested group?</p>");
  $("#pagelet_ego_contextual_group").remove();
 },
 "recommended-events": function () {
   $("._5_xt").closest("._5jmm").remove();
 },
 "suggested-pages": function () {
  // $("._4_u2").before("<p>found us a suggested page?</p>");
  $("#pagelet_ego_pane").remove();
 },
 "panel-tip": function () {
  // $("#aymt_homepage_panel_tip").before("<p>found us a panel tip?</p>");
  $("#aymt_homepage_panel_tip").remove();
 },
 "trending-topics": function () {
  // $("#pagelet_trending_tags_and_topics").before("<p>found us some trending topics?</p>");
  $("#pagelet_trending_tags_and_topics").remove();
 },
 "ego-pane": function () {
  // $("._5qrt, .cartRightCol").before("<p>found us some right sidebar ads?</p>");
  // $("._5qrt").remove();
  // $("#pagelet_side_ads").remove();
  $("#pagelet_ego_pane").remove();
 },
 "facebook-watch": function () {
  $("#pagelet_video_home_suggested_for_you_rhc").remove();
 },
 "autoplay-video": function () {
  $("._9_m").remove();
  $("video[autoplay]").closest("._4-u3").css("border", "5px solid orange");
  $("._4eeo").css("border", "3px solid orange");
 },
 "marketplace": function () {
   // $("._3u8m").parent().parent().parent().parent().parent().parent().parent().css("border", "3px solid green");
   // console.log($("._3u8m").parent().parent().parent().parent().parent().parent().parent().attr("class"));
   $("._1-ia:has(._3u8m)").remove();
 },
 "fix-links": function () {
  $("a").removeAttr("data-lynx-uri");
  // $("a").removeAttr("onhover");
  // $("a").removeAttr("onclick");
 },
 "chain-letters": function () {
  // TODO: make this work somehow
  $(".userContent").each(function () {
    var text = $(this).text();
    if (text.match(/paste.*don'?t.*share/)) {
     $(this).closest("._5jmm").fadeOut();
    }
  });
 },
 "hi-contrast": function () {
  $("._2t-f").css("background-color", "#91B8E1");
  $("._2s25").css("color", "black");
 },
 "rentals": function () {
  $("#rightColumn._3-96").remove();
 }
};
 
// page alteration script
function alter(mutation) {

 // Set up turned-on features:
 Object.keys(optionCallbacks).forEach(function (optionID) {
   setupOption(optionID, optionCallbacks[optionID]);
 });

}

// create an observer instance
//var observer = new WebKitMutationObserver(alter);
var observer = new MutationObserver(function(mutations) {
  mutations.forEach(alter);
});
 
// configuration of the observer:
var config = { attributes: true, childList: true, characterData: true };
 
// pass in the target node, as well as the observer options
observer.observe(target, config);

// alter the page whether or not it mutates
alter();
 
// later, you can stop observing
// observer.disconnect();

